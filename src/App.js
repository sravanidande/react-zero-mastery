import React from "react";
import CardList from "./components/CardList";
import "./components/Card.styles.css";
import "./App.css";
class App extends React.Component {
  constructor() {
    super();
    this.state = {
      monsters: [],
      search: ""
    };
  }

  handleChange = evt => {
    this.setState({ search: evt.target.value });
  };

  componentDidMount() {
    fetch("https://jsonplaceholder.typicode.com/users")
      .then(res => res.json())
      .then(users => this.setState({ monsters: users }));
  }
  render() {
    const { monsters, search } = this.state;
    const filteredMonsters = monsters.filter(monster =>
      monster.name.toLowerCase().includes(search.toLowerCase())
    );
    return (
      <div className="App">
        <h1>Monsters Rolodex</h1>
        <input
          type="search"
          placeholder="search monster"
          value={search}
          onChange={this.handleChange}
        />
        <CardList monsters={filteredMonsters} />
      </div>
    );
  }
}

export default App;
